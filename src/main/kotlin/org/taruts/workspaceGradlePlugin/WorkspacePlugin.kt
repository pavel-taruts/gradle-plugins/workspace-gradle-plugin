package org.taruts.workspaceGradlePlugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.TaskCollection
import org.slf4j.LoggerFactory
import org.taruts.processUtils.ProcessRunner
import org.taruts.workspaceGradlePlugin.file.CloneTaskFileLoader

class WorkspacePlugin : Plugin<Project> {

    private val log = LoggerFactory.getLogger(javaClass)

    private lateinit var cloneTaskFileLoader: CloneTaskFileLoader

    fun init(cloneTaskFileLoader: CloneTaskFileLoader) {
        this.cloneTaskFileLoader = cloneTaskFileLoader
    }

    override fun apply(project: Project) {

        val pluginActive: Boolean = hasGit(project) && hasGitRepo(project) && hasRemotes(project)

        val extension: WorkspacePluginExtension = project.extensions.create("workspace", WorkspacePluginExtension::class.java)

        if (!pluginActive) {
            return
        }

        // An aggregator task for all cloneX tasks
        // All cloneX tasks will be added to the cloneAll task dependencies upon creation
        project.tasks.register("cloneAll") {
            it.group = "workspace"
        }

        project.afterEvaluate {

            validateExtension(extension)

            val pluginContext = PluginContext(this, project, extension)
            extension.runRunnables()

            val cloneTasks: TaskCollection<CloneAdjacentGitRepositoryTask> = project.tasks.withType(CloneAdjacentGitRepositoryTask::class.java)
            if (cloneTasks.isEmpty()) {
                cloneTaskFileLoader.createCloneTasks()
                pluginContext.createRefreshProjectsListTask()
            }
        }
    }

    private fun hasGit(project: Project): Boolean {
        val success: Boolean = ProcessRunner.isSuccess {
            ProcessRunner.runProcess(project.projectDir, "git", "--version")
        }
        if (!success) {
            log.error("Gradle plugin org.taruts.workspace is inactive as there is no git on your operating system")
        }
        return success
    }

    private fun hasGitRepo(project: Project): Boolean {
        val success: Boolean = ProcessRunner.isSuccess {
            ProcessRunner.runProcess(project.projectDir, "git", "status")
        }
        if (!success) {
            log.error("Gradle plugin org.taruts.workspace is inactive because the local git repository is not initialized")
        }
        return success
    }

    private fun hasRemotes(project: Project): Boolean {
        val remotesStr = ProcessRunner.runProcess(project.projectDir, "git", "remote")
        val success: Boolean = remotesStr.isNotBlank()
        if (!success) {
            log.error("Gradle plugin org.taruts.workspace is inactive as there are no remotes in the local git repository")
        }
        return success
    }

    private fun validateExtension(extension: WorkspacePluginExtension) {
        val groupSeparator: String = extension.gitHub.groupSeparator.get()
        if (!groupSeparator.matches(WorkspacePluginExtension.GitHub.groupSeparatorValidationRegex)) {
            error("WorkspacePluginExtension.GitHub.getGroupSeparator does not match ${WorkspacePluginExtension.GitHub.groupSeparatorValidationRegex}")
        }
    }
}
