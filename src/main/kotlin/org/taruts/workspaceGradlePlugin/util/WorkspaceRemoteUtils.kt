package org.taruts.workspaceGradlePlugin.util

import org.gradle.api.Project
import org.taruts.processUtils.ProcessRunner

class WorkspaceRemoteUtils(private val project: Project) {

    fun getWorkspaceRemoteUrl(): String {
        val remote: String = getWorkspaceRemote()
        return ProcessRunner.runProcess(project.projectDir, "git", "remote", "get-url", remote)
    }

    private fun getWorkspaceRemote(): String {
        val remotes: List<String> = getWorkspaceRemotes()
        if (remotes.size != 1) {
            val remotesStr: String = remotes.joinToString(", ")
            error("There expected to be just one remote, but found ${remotes.size}: $remotesStr")
        }
        return remotes.single()
    }

    private fun getWorkspaceRemotes(): List<String> {
        val remotesStr: String = ProcessRunner.runProcess(project.projectDir, "git", "remote")
        return remotesStr.lines()
    }
}
