package org.taruts.workspaceGradlePlugin.util

object RemoteUtils {

    fun addDotGit(str: String): String {
        val addition = if (str.endsWith(".git"))
            "" else ".git"
        return str + addition
    }

    fun removeDotGit(str: String): String {
        return if (str.endsWith(".git"))
            str.substring(0, str.length - ".git".length)
        else str

    }
}
