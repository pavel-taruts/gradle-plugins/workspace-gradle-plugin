package org.taruts.workspaceGradlePlugin.taskFactory

import org.gradle.api.Project
import org.taruts.gitUtils.GitRemoteUrlParser
import org.taruts.workspaceGradlePlugin.CloneAdjacentGitRepositoryTask
import org.taruts.workspaceGradlePlugin.WorkspacePluginExtension
import org.taruts.workspaceGradlePlugin.util.RemoteUtils

class TaskFactory(
    private val project: Project,
    private val extension: WorkspacePluginExtension,
    private val gitUtilityCredentials: Array<String?>,
    private val clonedRemoteUtils: ClonedRemoteUtils,
    private val workspaceRootPath: String
) {
    fun createWithPathPostfix(repositoryPostfix: String) {
        val directoryRelativePath: String = urlPostfixToRelativePath(repositoryPostfix)
        createWithPathPostfix(repositoryPostfix, directoryRelativePath)
    }

    fun createWithPathPostfix(repositoryPostfix: String, directoryRelativePath: String) {
        val taskNamePostfix: String = clonedRemoteUtils.directoryRelativePathToPascalCase(directoryRelativePath)
        createWithPathPostfix(taskNamePostfix, repositoryPostfix, directoryRelativePath)
    }

    fun createWithPathPostfix(taskNamePostfix: String, adjacentRepositoryPathPostfix: String, directoryRelativePath: String) {
        val clonedRemoteUrl: String = clonedRemoteUtils.getClonedRemoteUrl(adjacentRepositoryPathPostfix)
        create(taskNamePostfix, clonedRemoteUrl, directoryRelativePath)
    }

    fun create(clonedRemoteUrl: String) {
        val directoryRelativePath: String = clonedRemoteUrlToRelativePath(clonedRemoteUrl)
        val taskNamePostfix: String = clonedRemoteUtils.directoryRelativePathToPascalCase(directoryRelativePath)
        create(taskNamePostfix, clonedRemoteUrl, directoryRelativePath)
    }

    private fun clonedRemoteUrlToRelativePath(clonedRemoteUrl: String): String {
        val clonedRemoteUrlPostfixDotGit: String = clonedRemoteUrlToPostfixDotGit(clonedRemoteUrl)
        return urlPostfixToRelativePath(clonedRemoteUrlPostfixDotGit)
    }

    private fun clonedRemoteUrlToPostfixDotGit(clonedRemoteUrl: String): String {
        val clonedRemoteUrlPathWithExtension: String = GitRemoteUrlParser.extractPath(clonedRemoteUrl)
        if (!clonedRemoteUrlPathWithExtension.startsWith(workspaceRootPath)) {
            throw BadPrefixException("The path of $clonedRemoteUrl was expected to start with $workspaceRootPath")
        }
        return clonedRemoteUrlPathWithExtension.substring(workspaceRootPath.length)
    }

    private fun urlPostfixToRelativePath(urlPostfix: String): String {
        val clonedRemoteUrlPathPostfixWithoutExtension = RemoteUtils.removeDotGit(urlPostfix)
        val directoryFactory = extension.directoryFactory.get()
        return directoryFactory.get(clonedRemoteUrlPathPostfixWithoutExtension)
    }

    private fun create(taskNamePostfix: String, clonedRemoteUrl: String, directoryRelativePath: String) {
        val taskName = "clone$taskNamePostfix"
        val taskProvider = project.tasks.register(taskName, CloneAdjacentGitRepositoryTask::class.java) {
            it.extension = extension
            it.clonedRemoteUrl = clonedRemoteUrl
            it.directoryRelativePath = directoryRelativePath
            it.gitUtilityCredentials = gitUtilityCredentials
        }
        project.tasks.named("cloneAll").configure {
            it.dependsOn(taskProvider)
        }
    }

    class BadPrefixException(message: String) : Exception(message)
}
