package org.taruts.workspaceGradlePlugin.taskFactory

import org.apache.commons.lang3.StringUtils
import org.taruts.workspaceGradlePlugin.util.RemoteUtils

class ClonedRemoteUtils(private val workspaceRootPrefix: String) {

    fun getClonedRemoteUrl(adjacentRemoteUrlPathPostfix: String): String {
        return workspaceRootPrefix + RemoteUtils.addDotGit(adjacentRemoteUrlPathPostfix)
    }

    fun directoryRelativePathToPascalCase(directoryRelativePath: String): String {
        return directoryRelativePath
            .split("[^\\p{Alnum}]+".toRegex())
            .asSequence()
            .filter(StringUtils::isNotBlank)
            .map { it.lowercase().replaceFirstChar(Char::titlecase) }
            .joinToString("")
    }
}
