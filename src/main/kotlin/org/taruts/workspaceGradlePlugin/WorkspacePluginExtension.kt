package org.taruts.workspaceGradlePlugin

import org.gradle.api.Action
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import org.taruts.workspaceGradlePlugin.directory.DirectoryFactory
import org.taruts.workspaceGradlePlugin.directory.impl.DefaultDirectoryFactory
import org.taruts.workspaceGradlePlugin.taskFactory.TaskFactory

abstract class WorkspacePluginExtension {
    @get:Nested
    abstract val gitHub: GitHub

    abstract val workspaceRemoteUrlPathPostfix: Property<String>

    abstract val projectsParentDirectoryRelativePath: Property<String>

    abstract val directoryFactory: Property<DirectoryFactory>

    init {
        projectsParentDirectoryRelativePath.convention("projects")
        directoryFactory.convention(DefaultDirectoryFactory())
    }

    private val runnables = mutableListOf<Runnable>()

    private lateinit var taskFactory: TaskFactory

    @Suppress("MemberVisibilityCanBePrivate")
    // This is for integration with org.taruts.djig
    lateinit var apiCredentials: Array<String?>

    fun init(taskFactory: TaskFactory, apiCredentials: Array<String?>) {
        this.taskFactory = taskFactory
        this.apiCredentials = apiCredentials
    }

    fun gitHub(action: Action<GitHub>) {
        action.execute(gitHub)
    }

    fun workspaceProject(repositoryPostfix: String) {
        runnables.add {
            taskFactory.createWithPathPostfix(repositoryPostfix)
        }
    }

    fun workspaceProject(repositoryPostfix: String, directoryRelativePath: String) {
        runnables.add {
            taskFactory.createWithPathPostfix(repositoryPostfix, directoryRelativePath)
        }
    }

    fun workspaceProject(taskNamePostfix: String, adjacentRepositoryPathPostfix: String, directoryRelativePath: String) {
        runnables.add {
            taskFactory.createWithPathPostfix(taskNamePostfix, adjacentRepositoryPathPostfix, directoryRelativePath)
        }
    }

    fun runRunnables() {
        runnables.forEach(Runnable::run)
    }

    abstract class GitHub {
        companion object {
            val groupSeparatorValidationRegex = "[^\\p{Alnum}]+".toRegex()
        }

        abstract val groupSeparator: Property<String>

        init {
            groupSeparator.convention(".")
        }
    }
}
