package org.taruts.workspaceGradlePlugin

import org.gradle.api.Project
import org.taruts.gitUtils.GitRemoteUrlParser
import org.taruts.gitUtils.hosting.HostingNameExtractor
import org.taruts.gitUtils.hosting.credentials.CredentialsUtils
import org.taruts.workspaceGradlePlugin.credentials.CredentialsProvider
import org.taruts.workspaceGradlePlugin.credentials.PropertiesLoader
import org.taruts.workspaceGradlePlugin.credentials.VariableValueProvider
import org.taruts.workspaceGradlePlugin.directory.DirectoryFactory
import org.taruts.workspaceGradlePlugin.file.CloneTaskFileLoader
import org.taruts.workspaceGradlePlugin.file.ProjectListService
import org.taruts.workspaceGradlePlugin.hosting.type.HostingTypeDetector
import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeListServiceSelector
import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeService
import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeServiceSelector
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.HubListService
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.HubService
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.LabListService
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.LabService
import org.taruts.workspaceGradlePlugin.taskFactory.ClonedRemoteUtils
import org.taruts.workspaceGradlePlugin.taskFactory.TaskFactory
import org.taruts.workspaceGradlePlugin.util.RemoteUtils
import org.taruts.workspaceGradlePlugin.util.WorkspaceRemoteUtils
import java.util.*

class PluginContext(plugin: WorkspacePlugin, private val project: Project, private val extension: WorkspacePluginExtension) {

    private val propertiesLoader = PropertiesLoader(project)
    private val properties: Properties? = propertiesLoader.loadProperties()

    private val workspaceRemoteUtils = WorkspaceRemoteUtils(project)
    private val workspaceRemoteUrl: String = workspaceRemoteUtils.getWorkspaceRemoteUrl()

    private val hostingType = HostingTypeDetector.detect(workspaceRemoteUrl)

    private val hubService = HubService(extension)
    private val labService = LabService()
    private val hostingTypeServiceSelector = HostingTypeServiceSelector(hubService, labService)
    private val hostingTypeService = hostingTypeServiceSelector.select(hostingType)

    private val workspaceRepositoryPostfix: String = calculateWorkspaceRepositoryPostfix(workspaceRemoteUrl, hostingTypeService)
    private val workspaceRootUrl: String = calculateWorkspaceRootPrefix(workspaceRemoteUrl, workspaceRepositoryPostfix)
    private val workspaceRootPath: String = GitRemoteUrlParser.extractPath(workspaceRootUrl)

    private val hostingName: String = HostingNameExtractor.extractHostingName(workspaceRemoteUrl)
    private val variableValueProvider = VariableValueProvider(project, properties, hostingName)
    private val credentialsProvider = CredentialsProvider(variableValueProvider)
    private val apiCredentials = credentialsProvider.getApiCredentials()
    private val gitUtilityCredentials: Array<String?> = CredentialsUtils.apiCredentialsToGitUtilityCredentials(apiCredentials[0], apiCredentials[1])

    private val hubListService = HubListService(workspaceRemoteUrl, apiCredentials, workspaceRootPath)
    private val labListService = LabListService(workspaceRemoteUrl, apiCredentials, workspaceRootPath)
    private val hostingTypeListServiceSelector = HostingTypeListServiceSelector(hubListService, labListService)
    private val hostingTypeListService = hostingTypeListServiceSelector.select(hostingType)

    private val clonedRemoteUtils = ClonedRemoteUtils(workspaceRootUrl)
    private val taskFactory = TaskFactory(project, extension, gitUtilityCredentials, clonedRemoteUtils, workspaceRootPath)

    private val projectListService = ProjectListService(project, hostingTypeListService)
    private val cloneTaskFileLoader = CloneTaskFileLoader(projectListService, taskFactory, workspaceRootUrl)

    init {
        plugin.init(cloneTaskFileLoader)
        extension.init(taskFactory, apiCredentials)

        initDirectoryFactory()
    }

    private fun initDirectoryFactory() {
        val directoryFactory: DirectoryFactory = extension.directoryFactory.get()
        directoryFactory.init(hostingTypeService)
    }

    fun createRefreshProjectsListTask() {
        project.tasks.register("refreshProjectsList", RefreshProjectsListTask::class.java) {
            it.projectListService = projectListService
        }
    }

    private fun calculateWorkspaceRepositoryPostfix(workspaceRemoteUrl: String, hostingTypeService: HostingTypeService): String {
        return extension.workspaceRemoteUrlPathPostfix.orNull ?: deduceWorkspaceRepositoryPostfixFromUrl(workspaceRemoteUrl, hostingTypeService)
    }

    private fun deduceWorkspaceRepositoryPostfixFromUrl(workspaceRemoteUrl: String, hostingTypeService: HostingTypeService): String {
        val workspaceRemotePathWithExtension: String = GitRemoteUrlParser.extractPath(workspaceRemoteUrl)
        val workspaceRemotePathWithoutExtension = RemoteUtils.removeDotGit(workspaceRemotePathWithExtension)

        return if (workspaceRemotePathWithoutExtension.contains(hostingTypeService.groupSeparator)) {
            workspaceRemotePathWithoutExtension.substringAfterLast(hostingTypeService.groupSeparator)
        } else {
            workspaceRemotePathWithoutExtension.substringAfterLast("/")
        }
    }

    private fun calculateWorkspaceRootPrefix(workspaceRemoteUrl: String, workspaceRepositoryPostfix: String): String {
        val workspaceRepositoryPostfixWithExtension: String = RemoteUtils.addDotGit(workspaceRepositoryPostfix)
        if (!workspaceRemoteUrl.endsWith(workspaceRepositoryPostfixWithExtension)) {
            error("Workspace remote $workspaceRemoteUrl does not end with the configured postfix $workspaceRepositoryPostfixWithExtension")
        }
        return workspaceRemoteUrl.substring(0, workspaceRemoteUrl.length - workspaceRepositoryPostfixWithExtension.length)
    }
}
