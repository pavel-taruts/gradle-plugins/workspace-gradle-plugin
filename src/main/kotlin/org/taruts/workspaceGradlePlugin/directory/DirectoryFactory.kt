package org.taruts.workspaceGradlePlugin.directory

import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeService

abstract class DirectoryFactory {

    protected lateinit var hostingTypeService: HostingTypeService

    fun init(hostingTypeService: HostingTypeService) {
        this.hostingTypeService = hostingTypeService
    }

    abstract fun get(urlPathPostfix: String): String
}
