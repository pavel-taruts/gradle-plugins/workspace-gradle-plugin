package org.taruts.workspaceGradlePlugin.directory.impl

import java.io.File

class DefaultDirectoryFactory : ReplaceDirectoryFactory(File.separator)
