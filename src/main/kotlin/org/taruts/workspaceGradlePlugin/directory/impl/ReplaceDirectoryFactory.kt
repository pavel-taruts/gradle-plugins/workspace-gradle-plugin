package org.taruts.workspaceGradlePlugin.directory.impl

import org.taruts.workspaceGradlePlugin.directory.DirectoryFactory

open class ReplaceDirectoryFactory(private val substitute: String) : DirectoryFactory() {

    override fun get(urlPathPostfix: String): String {
        return urlPathPostfix.replace(hostingTypeService.groupSeparator, substitute)
    }
}
