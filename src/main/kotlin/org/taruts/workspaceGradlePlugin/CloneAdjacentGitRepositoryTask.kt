package org.taruts.workspaceGradlePlugin

import org.apache.commons.io.FileUtils
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.taruts.gitUtils.GitUtils
import java.io.File

/**
 * This task clones another Git repository of the same group as this project itself into a subdirectory.
 *
 * The project which uses this plugin is considered a workspace project, it's only purpose is to group other projects together.
 *
 * You clone this workspace project first, and you do it manually.
 *
 * Other projects, which the workspace has, are cloned to subdirectories.
 *
 * They are cloned automatically, each by a separate instance of this task.
 */
abstract class CloneAdjacentGitRepositoryTask : DefaultTask() {

    @Input
    var extension: WorkspacePluginExtension? = null
        set(value) {
            field = value
            updateDescription()
        }

    @Input
    var clonedRemoteUrl: String? = null
        set(value) {
            field = value
            updateDescription()
        }

    /**
     * The subdirectory inside the workspace project to clone the other project to.
     */
    @Input
    var directoryRelativePath: String? = null
        set(value) {
            field = value
            updateDescription()
        }

    @Input
    lateinit var gitUtilityCredentials: Array<String?>

    init {
        group = "workspace"
    }

    private fun updateDescription() {
        val projectsParentDirectoryRelativePath: String? = extension?.projectsParentDirectoryRelativePath?.get()
        description =
            "Clones $clonedRemoteUrl into subdirectory $projectsParentDirectoryRelativePath${File.separator}$directoryRelativePath"
    }

    @TaskAction
    fun action() {
        val subdirectoryToCloneTo: File = getSubdirectoryToCloneTo()
        val (username, password) = gitUtilityCredentials
        GitUtils.forceClone(clonedRemoteUrl!!, username, password, null, subdirectoryToCloneTo)
    }

    private fun getSubdirectoryToCloneTo(): File {
        val directoryRelativePath: String = this.directoryRelativePath!!
        val projectsParentDirectoryRelativePath: String = extension!!.projectsParentDirectoryRelativePath.get()
        return FileUtils.getFile(project.rootDir, projectsParentDirectoryRelativePath, directoryRelativePath)
    }
}
