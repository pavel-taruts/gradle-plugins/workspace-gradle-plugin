package org.taruts.workspaceGradlePlugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.taruts.workspaceGradlePlugin.file.ProjectListService

abstract class RefreshProjectsListTask : DefaultTask() {

    @Input
    lateinit var projectListService: ProjectListService

    init {
        group = "workspace"
    }

    @TaskAction
    fun action() {
        projectListService.refreshFile()
    }
}
