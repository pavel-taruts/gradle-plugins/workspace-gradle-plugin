package org.taruts.workspaceGradlePlugin.hosting.api

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import java.net.URI

object GitLabApiFactory {
    /**
     * Gets an authenticated [GitLabApi] by credentials.
     *
     * If both [username] and [password] are specified, then the login-password authentication is used.
     *
     * If there is only [username] then it's expected to be a personal access token (PAT).
     */
    fun getGitLabApi(hostUrl: String, username: String, password: String?): GitLabApi {
        var api: GitLabApi = getGitLabApiInternal(hostUrl, username, password)
        var uri = URI(hostUrl)
        if (uri.scheme == "http") {
            try {
                api.version
            } catch (e: GitLabApiException) {
                if (e.httpStatus == 301) {
                    uri = URI("https", uri.userInfo, uri.host, uri.port, uri.path, uri.query, uri.fragment)
                    api = getGitLabApiInternal(uri.toString(), username, password)
                }
            }
        }
        return api
    }

    private fun getGitLabApiInternal(hostUrl: String, username: String, password: String?): GitLabApi {
        return if (password.isNullOrBlank()) {
            val personalAccessToken: String = username
            GitLabApi(hostUrl, personalAccessToken)
        } else {
            GitLabApi.oauth2Login(hostUrl, username, password, true)
        }
    }
}
