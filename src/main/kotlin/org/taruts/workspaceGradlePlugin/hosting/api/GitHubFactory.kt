package org.taruts.workspaceGradlePlugin.hosting.api

import org.kohsuke.github.GitHub
import org.kohsuke.github.GitHubBuilder

object GitHubFactory {
    /**
     * Gets an authenticated GitHub API by credentials.
     *
     * If both [username] and [password] are specified, then the login-password authentication is used.
     *
     * If there is only [username] then it's expected to be a personal access token (PAT).
     *
     * In theory, it should also work with a OAuth2 token in [username], but we don't use OAuth2 in this project.
     */
    fun getGitHub(username: String?, password: String?): GitHub {
        return if (password.isNullOrBlank()) {
            GitHubBuilder().withOAuthToken(username).build()
        } else {
            GitHubBuilder().withPassword(username, password).build()
        }
    }
}
