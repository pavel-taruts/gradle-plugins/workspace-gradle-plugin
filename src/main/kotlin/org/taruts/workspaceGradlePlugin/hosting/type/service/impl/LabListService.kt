package org.taruts.workspaceGradlePlugin.hosting.type.service.impl

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.GroupProjectsFilter
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.ProjectFilter
import org.taruts.gitUtils.GitRemoteUrlParser
import org.taruts.workspaceGradlePlugin.hosting.api.GitLabApiFactory
import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeListService
import org.taruts.workspaceGradlePlugin.util.RemoteUtils
import java.net.URI

class LabListService(
    private val workspaceRemoteUrl: String,
    private val apiCredentials: Array<String?>,
    private val workspaceRootPath: String
) : HostingTypeListService {

    override fun getList(): List<String> {
        val gitLabApi: GitLabApi = getApi()
        val workspaceProject: Project = getWorkspaceProject(gitLabApi)
        val kind: String = workspaceProject.namespace.kind
        val projectSuperset: List<Project> = getProjectSuperset(gitLabApi, kind)
        return getWorkspaceProjectUrls(projectSuperset)
    }

    private fun getApi(): GitLabApi {
        val (username, password) = apiCredentials
        if (username.isNullOrBlank()) {
            throw NullPointerException(
                "username must be specified. " +
                        "It can also contain a personal access token. " +
                        "Consider consulting the docs on how to provide hosting credentials"
            )
        }
        val hostingUri: URI = GitRemoteUrlParser.extractHostingUri(workspaceRemoteUrl)
        return GitLabApiFactory.getGitLabApi(hostingUri.toString(), username, password)
    }

    private fun getWorkspaceProject(gitLabApi: GitLabApi): Project {
        val workspaceProjectPath: String = getWorkspaceProjectPath()
        return gitLabApi.projectApi.getProject(workspaceProjectPath)
    }

    private fun getWorkspaceProjectPath(): String {
        var workspaceUrlPath: String = GitRemoteUrlParser.extractPath(workspaceRemoteUrl)
        // Remove the first slash
        workspaceUrlPath = workspaceUrlPath.substring(1)
        return RemoteUtils.removeDotGit(workspaceUrlPath)
    }

    private fun getProjectSuperset(gitLabApi: GitLabApi, kind: String): List<Project> {
        val workspaceRootNamespace: String = getWorkspaceRootNamespace()
        val projects: List<Project> = when (kind) {
            "user" -> {
                val user: String = workspaceRootNamespace
                gitLabApi.projectApi.getUserProjects(
                    user,
                    ProjectFilter()
                        .withSimple(true)
                )
            }

            "group" -> {
                val group: String = workspaceRootNamespace
                gitLabApi.groupApi.getProjects(
                    group,
                    GroupProjectsFilter()
                        .withSimple(true)
                        .withIncludeSubGroups(true)
                )
            }

            else -> {
                error("Bad namespace.kind $kind")
            }
        }
        return projects
    }

    private fun getWorkspaceRootNamespace(): String {
        val workspaceRootNamespace: String = workspaceRootPath.substringBeforeLast("/", "")
        if (workspaceRootNamespace.isBlank()) {
            error(
                "Workspace root path is $workspaceRootPath. It should have two slashes, but has just one. " +
                        "This is probably because " +
                        "WorkspacePluginExtension.getWorkspaceRemoteUrlPathPostfix " +
                        "has an incorrect value that cuts off too much from $workspaceRootPath."
            )
        }
        // Removing the first slash
        return workspaceRootNamespace.substring(1)
    }

    private fun getWorkspaceProjectUrls(projectSuperset: List<Project>): List<String> {
        val workspaceProjectUrlPath: String = GitRemoteUrlParser.extractPath(workspaceRemoteUrl)
        return projectSuperset
            .asSequence()
            .mapNotNull(Project::getHttpUrlToRepo)
            .filter { urlStr ->
                val path: String = URI(urlStr).path
                path.startsWith(workspaceRootPath) && path != workspaceProjectUrlPath
            }
            .toList()
    }
}
