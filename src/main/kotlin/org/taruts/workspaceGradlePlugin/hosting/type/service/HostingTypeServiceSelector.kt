package org.taruts.workspaceGradlePlugin.hosting.type.service

import org.taruts.workspaceGradlePlugin.hosting.type.HostingType
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.HubService
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.LabService

class HostingTypeServiceSelector(private val hubService: HubService, private val labService: LabService) {

    fun select(hostingType: HostingType): HostingTypeService {
        return when (hostingType) {
            HostingType.GitHub -> hubService
            HostingType.GitLab -> labService
        }
    }
}
