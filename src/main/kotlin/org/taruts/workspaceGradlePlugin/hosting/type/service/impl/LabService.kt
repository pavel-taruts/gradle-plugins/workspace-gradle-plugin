package org.taruts.workspaceGradlePlugin.hosting.type.service.impl

import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeService

class LabService : HostingTypeService {

    override val groupSeparator: String
        get() = "/"

}
