package org.taruts.workspaceGradlePlugin.hosting.type.service.impl

import org.kohsuke.github.GHRepository
import org.kohsuke.github.GitHub
import org.kohsuke.github.PagedIterable
import org.taruts.gitUtils.GitRemoteUrlParser
import org.taruts.workspaceGradlePlugin.hosting.api.GitHubFactory
import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeListService
import java.net.URI

class HubListService(
    private val workspaceRemoteUrl: String,
    private val apiCredentials: Array<String?>,
    private val workspaceRootPath: String
) : HostingTypeListService {

    override fun getList(): List<String> {
        val ghRepositoriesForOwner: List<GHRepository> = getGhRepositoriesWithAccess()
        return getWorkspaceProjectUrls(ghRepositoriesForOwner)
    }

    private fun getGhRepositoriesWithAccess(): List<GHRepository> {
        val gitHub: GitHub = getGitHub()
        val pagedIterable: PagedIterable<GHRepository> = gitHub.myself.listRepositories(100)
        return pagedIterable.toList()
    }

    private fun getGitHub(): GitHub {
        val (username, password) = apiCredentials
        return GitHubFactory.getGitHub(username, password)
    }

    private fun getWorkspaceProjectUrls(ghRepositorySuperset: List<GHRepository>): List<String> {
        val workspaceProjectUrlPath: String = GitRemoteUrlParser.extractPath(workspaceRemoteUrl)
        return ghRepositorySuperset
            .asSequence()
            .mapNotNull(GHRepository::getHttpTransportUrl)
            .filter { urlStr ->
                val path: String = URI(urlStr).path
                val startsWith = path.startsWith(workspaceRootPath)
                val isWorkspace = path != workspaceProjectUrlPath
                val ok = startsWith && isWorkspace
                ok
            }
            .toList()
    }
}
