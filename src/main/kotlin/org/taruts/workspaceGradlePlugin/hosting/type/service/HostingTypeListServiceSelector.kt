package org.taruts.workspaceGradlePlugin.hosting.type.service

import org.taruts.workspaceGradlePlugin.hosting.type.HostingType
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.HubListService
import org.taruts.workspaceGradlePlugin.hosting.type.service.impl.LabListService

class HostingTypeListServiceSelector(private val hubListService: HubListService, private val labListService: LabListService) {

    fun select(hostingType: HostingType): HostingTypeListService {
        return when (hostingType) {
            HostingType.GitHub -> hubListService
            HostingType.GitLab -> labListService
        }
    }
}
