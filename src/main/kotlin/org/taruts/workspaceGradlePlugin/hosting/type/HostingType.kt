package org.taruts.workspaceGradlePlugin.hosting.type

enum class HostingType {
    GitHub, GitLab
}