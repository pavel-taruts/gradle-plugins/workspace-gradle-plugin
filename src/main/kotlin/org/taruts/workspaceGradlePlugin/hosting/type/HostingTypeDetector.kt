package org.taruts.workspaceGradlePlugin.hosting.type

import org.taruts.gitUtils.GitRemoteUrlParser
import java.net.URI

object HostingTypeDetector {
    fun detect(url: String): HostingType {
        val uri: URI = GitRemoteUrlParser.extractHostingUri(url)
        return detect(uri)
    }

    private fun detect(uri: URI): HostingType {
        return if ("github.com" == uri.host) {
            HostingType.GitHub
        } else {
            HostingType.GitLab
        }
    }
}
