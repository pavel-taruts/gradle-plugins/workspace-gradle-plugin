package org.taruts.workspaceGradlePlugin.hosting.type.service

interface HostingTypeService {

    val groupSeparator: String
}
