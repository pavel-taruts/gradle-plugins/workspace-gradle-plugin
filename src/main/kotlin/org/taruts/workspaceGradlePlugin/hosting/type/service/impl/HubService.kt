package org.taruts.workspaceGradlePlugin.hosting.type.service.impl

import org.taruts.workspaceGradlePlugin.WorkspacePluginExtension
import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeService

class HubService(
    private val extension: WorkspacePluginExtension
) : HostingTypeService {

    override val groupSeparator: String
        get() = extension.gitHub.groupSeparator.get()
}
