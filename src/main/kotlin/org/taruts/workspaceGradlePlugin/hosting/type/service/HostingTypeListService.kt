package org.taruts.workspaceGradlePlugin.hosting.type.service

interface HostingTypeListService {

    fun getList(): List<String>
}
