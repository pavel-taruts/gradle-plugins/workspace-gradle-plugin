package org.taruts.workspaceGradlePlugin.file

import org.apache.commons.io.FileUtils
import org.gradle.api.Project
import org.taruts.workspaceGradlePlugin.hosting.type.service.HostingTypeListService
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter

class ProjectListService(private val project: Project, private val hostingTypeListService: HostingTypeListService) {

    fun getFile(): File {
        val buildSubdirectory = FileUtils.getFile(project.buildDir, "org.taruts.workspace")
        buildSubdirectory.mkdirs()
        return File(buildSubdirectory, "projects.txt")
    }

    fun refreshFile() {
        val file = getFile()
        refreshFile(file)
    }

    fun refreshFile(file: File) {
        val repositoryUrls: List<String> = hostingTypeListService.getList()
        save(file, repositoryUrls)
    }

    private fun save(file: File, repositoryUrls: List<String>) {
        FileWriter(file, false).use { fileWriter ->
            BufferedWriter(fileWriter).use { bufferedWriter ->
                PrintWriter(bufferedWriter).use { printWriter ->
                    repositoryUrls.forEach { repositoryUrl ->
                        printWriter.println(repositoryUrl)
                    }
                }
            }
        }
    }
}
