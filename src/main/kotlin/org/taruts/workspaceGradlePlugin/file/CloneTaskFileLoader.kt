package org.taruts.workspaceGradlePlugin.file

import org.slf4j.LoggerFactory
import org.taruts.workspaceGradlePlugin.taskFactory.TaskFactory
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

class CloneTaskFileLoader(
    private val projectListService: ProjectListService,
    private val taskFactory: TaskFactory,
    private val workspaceRootPrefix: String
) {
    private val log = LoggerFactory.getLogger(javaClass)

    fun createCloneTasks() {
        val projectListFile = projectListService.getFile()
        if (!projectListFile.exists()) {
            projectListService.refreshFile(projectListFile)
        }
        readFile(projectListFile)
    }

    private fun readFile(file: File) {

        val badPrefixException = object {
            var value = false
        }

        FileReader(file).use { fileReader ->
            BufferedReader(fileReader).use { bufferedReader ->
                bufferedReader.lines().forEach { clonedRemoteUrl ->
                    try {
                        taskFactory.create(clonedRemoteUrl)
                    } catch (e: TaskFactory.BadPrefixException) {
                        badPrefixException.value = true
                    }
                }
            }
        }

        if (badPrefixException.value) {
            log.error("Paths of some URLs from $file do not start from $workspaceRootPrefix, as projects of this workspace should.")
            log.error("It must be that the file was formed when the extension settings were different from what we have now.")
            log.error("If you're running refreshProjectsList, just ignore this message.")
            log.error("If not, then run it and it'll rebuild $file.")
        }
    }
}
