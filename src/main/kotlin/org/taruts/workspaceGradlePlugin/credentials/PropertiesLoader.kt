package org.taruts.workspaceGradlePlugin.credentials

import org.gradle.api.Project
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.util.*

class PropertiesLoader(private val project: Project) {

    fun loadProperties(): Properties? {
        val file: File = project.file("org.taruts.workspace.private.properties")
        return if (file.exists()) {
            FileReader(file).use { fileReader ->
                BufferedReader(fileReader).use { bufferedReader ->
                    Properties().apply {
                        load(bufferedReader)
                    }
                }
            }
        } else {
            null
        }
    }
}
