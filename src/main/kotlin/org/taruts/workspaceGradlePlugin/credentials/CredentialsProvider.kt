package org.taruts.workspaceGradlePlugin.credentials

class CredentialsProvider(private val variableValueProvider: VariableValueProvider) {

    fun getApiCredentials(): Array<String?> {
        val apiUsername: String? = variableValueProvider.getVariableValue("username")
        val apiPassword: String? = variableValueProvider.getVariableValue("password")
        return arrayOf(apiUsername, apiPassword)
    }
}
