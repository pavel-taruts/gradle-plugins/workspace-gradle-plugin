package org.taruts.workspaceGradlePlugin.credentials

import org.apache.commons.lang3.StringUtils
import org.gradle.api.Project
import java.util.*

class VariableValueProvider(private val project: Project, private val properties: Properties?, private val hostingName: String) {

    fun getVariableValue(variableName: String): String? {
        return firstOrNull(variableName, ::getValueFromPropertiesFile, ::getValueFromExternalSources)
    }

    private fun getValueFromPropertiesFile(variableName: String): String? {
        return properties?.getProperty(variableName)
    }

    private fun getValueFromExternalSources(variableName: String): String? {
        return getPropertyPrefixes()
            .asSequence()
            .map { prefix ->
                val propertyName = "$prefix$variableName"
                getPropertyValue(propertyName)
            }
            .firstOrNull { propertyValue ->
                !propertyValue.isNullOrBlank()
            }
    }

    private fun getPropertyPrefixes(): List<String> {
        val firstPart = "org.taruts.workspace."
        val hostingNamePart = "$hostingName."
        val groupPart: String = project.group.toString().let {
            if (it.isBlank()) "" else "$it."
        }
        val namePart: String = project.name.let {
            if (it.isBlank()) "" else "$it."
        }
        return listOf(
            "$firstPart$hostingNamePart$groupPart$namePart",
            "$firstPart$groupPart$namePart",

            "$firstPart$hostingNamePart$groupPart",
            "$firstPart$groupPart",

            "$firstPart$hostingNamePart",
            firstPart,
        )
    }

    private fun getPropertyValue(propertyName: String): String? {
        return firstOrNull(propertyName, ::getFromSystemProperty, ::getFromProjectProperty, ::getFromEnvironmentVariable)
    }

    private fun getFromSystemProperty(propertyName: String): String? {
        val value: String? = System.getProperty(propertyName)
        return StringUtils.trimToNull(value)
    }

    private fun getFromProjectProperty(propertyName: String): String? {
        return if (project.hasProperty(propertyName)) {
            val value: String? = project.property(propertyName)?.toString()
            StringUtils.trimToNull(value)
        } else {
            null
        }
    }

    private fun getFromEnvironmentVariable(propertyName: String): String? {
        val environmentVariableName: String = EnvironmentVariableNameProvider.getName(propertyName)
        val value: String? = System.getenv(environmentVariableName)
        return StringUtils.trimToNull(value)
    }

    private fun firstOrNull(key: String, vararg provider: (String) -> String?): String? {
        return provider.asSequence()
            .map { it(key) }
            .firstOrNull { !it.isNullOrBlank() }
    }
}
