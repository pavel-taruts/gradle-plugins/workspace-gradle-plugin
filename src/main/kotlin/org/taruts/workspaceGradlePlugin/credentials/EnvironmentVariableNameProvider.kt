package org.taruts.workspaceGradlePlugin.credentials

object EnvironmentVariableNameProvider {

    fun getName(propertyName: String): String {
        return propertyName
            .replace("""[.\\-]+""".toRegex(), "_")
            .replace("""_+""".toRegex(), "_")
            .replace("""^_""".toRegex(), "")
            .replace("""_$""".toRegex(), "")
            .uppercase()
    }
}
