import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.10"
    `java-gradle-plugin`
    `maven-publish`
    id("com.gradle.plugin-publish") version "1.0.0"
}

group = "org.taruts"
version = "1.0.3"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.taruts:taruts-process-utils:1.0.3")
    implementation("org.taruts:taruts-git-utils:1.0.1")

    // Utils
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("commons-io:commons-io:2.11.0")
    implementation("com.google.guava:guava:31.1-jre")

    // Git repository hostings APIs
    implementation("org.gitlab4j:gitlab4j-api:5.0.1")
    implementation("org.kohsuke:github-api:1.308")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

pluginBundle {
    website = "https://gitlab.com/pavel-taruts/gradle-plugins/workspace-gradle-plugin"
    vcsUrl = "https://gitlab.com/pavel-taruts/gradle-plugins/workspace-gradle-plugin.git"
    tags = listOf("workspace", "group", "automation", "git", "clone", "subproject", "child")
}

gradlePlugin {
    plugins {
        create("workspace") {
            id = "org.taruts.workspace"
            displayName = "Workspace"

            description = """
            A plugin for workspace projects. 
            A workspace project groups a number of related projects, each stored in a separate Git repository. 
            This plugin automates cloning of projects included in a workspace 
            for developers who start working with it.
            The DSL of the plugin allows to specify the list of projects in the workspace 
            and to which subdirectories they are to be cloned.
            """.trimIndent()

            implementationClass = "org.taruts.workspaceGradlePlugin.WorkspacePlugin"
        }
    }
}
